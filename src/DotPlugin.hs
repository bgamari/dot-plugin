{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NamedFieldPuns #-}

module DotPlugin (plugin) where

import GHC
import GHC.Core (Bind(..))
import GHC.Core.Opt.Pipeline.Types
import GHC.Utils.Outputable (defaultSDocContext, renderWithContext)
import GHC.Unit.Module.ModGuts
import qualified GHC.Types.Name.Set as NameSet
import GHC.Types.Name (nameModule_maybe)
import GHC.Types.Name.Occurrence (occNameString)
import GHC.Driver.Plugins (defaultPlugin, CorePlugin, Plugin(..), PluginRecompile(..))

import Control.Monad.IO.Class

import Graph.Dot
import MakeGraph

plugin :: Plugin
plugin = defaultPlugin
    { pluginRecompile = const $ return NoForceRecompile
    , installCoreToDos = installCoreToDo
    }

installCoreToDo :: CorePlugin
installCoreToDo _ todos = return $ pluginToDo : todos

pluginToDo :: CoreToDo
pluginToDo = CoreDoPluginPass "dump dot" $ \guts -> do
    let interestingBndrs = NameSet.mkNameSet
            [ getName bndr
            | bind <- mg_binds guts
            , bndr <- case bind of
                        Rec bs -> map fst bs
                        NonRec b _ -> [b]
            ]
        interestingBndr b = b `NameSet.elemNameSet` interestingBndrs
        interestingOcc _ = True
        context = []
        env = Env {context, interestingBndr, interestingOcc}

    let graph = runGraphM env $ foldMap bindG (mg_binds guts)
        ctx = defaultSDocContext
        this_mod = mg_module guts
    liftIO $ writeFile "out.dot"
        $ renderWithContext ctx
        $ toDot nodeAttrs (nodeName this_mod) graph
    return guts

nodeName :: Module -> Node -> String
nodeName this_mod (NName n) = concat
    [ moduleNameString (moduleName modl)
    , "."
    , occNameString (getOccName n)
    ]
  where
    modl = case nameModule_maybe n of
             Just m -> m
             Nothing -> this_mod

nodeAttrs :: Node -> [(String, String)]
nodeAttrs _ = []
