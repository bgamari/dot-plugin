module Graph.Dot (toDot) where

import GHC.Utils.Outputable

import qualified Data.Set as S

import Graph

import Prelude hiding ((<>))

toDot :: Ord a
      => (a -> [(String, String)])
      -> (a -> String)
      -> Graph a
      -> SDoc
toDot nodeAttrs nodeName gr = 
    text "digraph" <+> braces (nest 2 $ vcat $
        map renderNode (S.toList $ nodes gr) ++
        map renderEdge (S.toList $ edges gr)
    )
  where
    node n = text (nodeName n)
    renderNode n = node n <> semi
    renderEdge (Edge s t) = node s <> text " -> " <> node t <> semi
