module Graph where

import qualified Data.Set as S

data Edge a = Edge { source, target :: !a }
    deriving (Eq, Ord, Show)

data Graph a = Graph { edges :: S.Set (Edge a) }

instance Ord a => Semigroup (Graph a) where
    Graph e1 <> Graph e2 = Graph (e1 <> e2)

instance Ord a => Monoid (Graph a) where
    mempty = Graph mempty

nodes :: Ord a => Graph a -> S.Set a
nodes gr = S.fromList
    [ n
    | Edge s t <- S.toList $ edges gr
    , n <- [s,t]
    ]
