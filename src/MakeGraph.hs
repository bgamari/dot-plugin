{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module MakeGraph where

import GHC
import GHC.Core (AltCon(..), Bind(..), Expr(..), Alt(..))
import GHC.Core.TyCo.Rep (Type(..), Coercion(..))

import Control.Monad.Trans.RWS.CPS
import qualified Data.Set as S
import Control.Monad (when)

import Prelude hiding (cos)

import Graph

data Node = NName !Name
    deriving (Eq, Ord)

data Env = Env { context :: [Node]
               , interestingBndr :: !(Name -> Bool)
               , interestingOcc :: !(Name -> Bool)
               }

newtype GraphM a = GraphM { unGraphM :: RWS Env (Graph Node) () a }
    deriving (Functor, Applicative, Monad)

runGraphM :: Env -> GraphM () -> Graph Node
runGraphM env (GraphM k) = snd $ evalRWS k env ()

instance Semigroup (GraphM ()) where
    (<>) = (>>)

instance Monoid (GraphM ()) where
    mempty = return ()

tellEdge :: Edge Node -> GraphM ()
tellEdge e = GraphM $ tell $ Graph {edges = S.singleton e}

binding :: Id -> GraphM a -> GraphM a
binding i = bindings [getName i]

bindings :: [Name] -> GraphM a -> GraphM a
bindings bs (GraphM act) = GraphM $ do
    local (\env ->
        let bs' = map NName $ filter (interestingBndr env) $ map getName bs
        in env { context = bs' ++ context env }) act

occurrence :: Name -> GraphM ()
occurrence n = do
    env <- GraphM ask
    when (interestingOcc env n) $ do
        mapM_ (\b -> tellEdge (Edge b (NName n))) (context env)

bindG :: Bind Id -> GraphM ()
bindG b = bindG' b mempty

bindG' :: Bind Id -> GraphM a -> GraphM a
bindG' (Rec bs) k = do
    foldMap (\(b,e) -> binding b $ exprG e) bs
    bindings (map (getName . fst) bs) k
bindG' (NonRec b e) k = binding b $ exprG e >> k

exprG :: Expr Id -> GraphM ()
exprG (Var i) = occurrence (getName i)
exprG (Lit _) = mempty
exprG (App x y) = exprG x <> exprG y
exprG (Lam b e) = binding b $ exprG e
exprG (Let b e) = bindG' b (exprG e)
exprG (Case e b _ alts) = exprG e <> binding b (foldMap altOccs alts)
exprG (Cast e co) = exprG e <> coercionG co
exprG (Tick _ e) = exprG e
exprG (Type ty) = typeG ty
exprG (Coercion co) = coercionG co

typeG :: Type -> GraphM ()
typeG (TyVarTy tv) = occurrence (getName tv)
typeG (AppTy x y) = typeG x <> typeG y
typeG (TyConApp tc tys) = occurrence (getName tc) <> foldMap typeG tys
typeG (ForAllTy _ ty) = typeG ty
typeG (FunTy _ _ arg res) = typeG arg <> typeG res
typeG (LitTy _) = mempty
typeG (CastTy ty co) = typeG ty <> coercionG co
typeG (CoercionTy co) = coercionG co

altOccs :: Alt Id -> GraphM ()
altOccs (Alt con _ e) = con_occs <> exprG e
  where
    con_occs = case con of
                 DataAlt dc -> occurrence (getName dc)
                 LitAlt _ -> mempty
                 DEFAULT -> mempty

coercionG :: Coercion -> GraphM ()
coercionG (Refl ty) = typeG ty
coercionG (GRefl _ ty _) = typeG ty
coercionG (TyConAppCo _ tc cos) = occurrence (getName tc) <> foldMap coercionG cos
coercionG (AppCo x y) = coercionG x <> coercionG y
coercionG (ForAllCo _ x y) = coercionG x <> coercionG y
coercionG co@(FunCo{}) = coercionG (fco_mult co) <> coercionG (fco_arg co) <> coercionG (fco_res co)
coercionG (CoVarCo v) = occurrence (getName v)
coercionG (AxiomInstCo ax _ cos) = occurrence (getName ax) <> foldMap coercionG cos
coercionG (AxiomRuleCo _ cos) = foldMap coercionG cos
coercionG (UnivCo _ _ x y) = typeG x <> typeG y
coercionG (SymCo co) = coercionG co
coercionG (TransCo x y) = coercionG x <> coercionG y
coercionG (SelCo _ co) = coercionG co
coercionG (LRCo _ co) = coercionG co
coercionG (InstCo x y) = coercionG x <> coercionG y
coercionG (KindCo co) = coercionG co
coercionG (SubCo co) = coercionG co
coercionG (HoleCo _co) = mempty

